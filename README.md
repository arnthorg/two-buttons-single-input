# two buttons single input on (some) AVR's

## Principle of operation

When either button is pushed, the voltage on the input goes below the input low voltage threshold. The micro will then enable the input_pullup resistor and if the voltage is pulled above the input high voltage threshold, it's registered as button2 click, otherwise button1. 

The input pull-up resistors of the ATmega 32u4 and 328 are 50k$`\Omega`$ worst case. 

## Input thresholds
#### ATmega328

V<sub>IL</sub> = <0.3V<sub>CC</sub> $`\approx`$ < 1.5V @ 5V

V<sub>IH</sub> = >0.6V<sub>CC</sub> $`\approx`$ > 3V @ 5V

#### ATmega32u4

V<sub>IL</sub> = <0.2V<sub>CC</sub> - 0.1V $`\approx`$ < 0.9V @ 5V

V<sub>IH</sub> = >0.2V<sub>CC</sub> + 0.9V $`\approx`$ > 1.9V @ 5V

## Resistor values
#### ATmega328

R1 = 330k$`\Omega`$

R2 = 100k$`\Omega`$

should work on the ATmega328 giving 1.2V out min and 3.5V out high but that hasn't been tested. 

#### ATmega32u4
R1 = 330k$`\Omega`$

R2 = 33k$`\Omega`$ 

Has been tested on an ATmega 32u4 as it gives .5V out when button is depressed and 2.5V out when button is depressed and internal pull-up resistor is engaged. These resistor values should give .5V min and 2.16V max, worst case, when the pull-up resistor is 50k$`\Omega`$ which should be enough to pass the input high/low thresholds.

## Schematic

![](schematic/drawing.png)

## Addendum

I used iterateVoltageDivider.py to find suitable resistor values for R1 and R2. 