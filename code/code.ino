#define buttonPin 2
#define ledPin 13
void setup() {
  // put your setup code here, to run once:
  Serial.begin(500000);
  while(!Serial);
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  buttonCheck();
}

void buttonCheck() {
  if(!digitalRead(buttonPin)) {
    //debounce
    delay(50); 
    if(digitalRead(buttonPin))
      return;
    pinMode(buttonPin, INPUT_PULLUP);
    delay(1); //wait for voltage to stabilize
    if(digitalRead(buttonPin)) {
      digitalWrite(ledPin, HIGH);
      Serial.println("ledpin high");
    }
    else {
      digitalWrite(ledPin, LOW);
      Serial.println("ledpin LOW");
    }
    pinMode(buttonPin, INPUT);
    while(!digitalRead(buttonPin));
  }
}

