#python3
Rpu = 20e3
Rpu2 = 50e3
Vin = 5
ViHmin328 = 3
ViLmax328 = 1.5
ViHmin32u4 = 1.9
ViLmax32u4 = 0.9

#ViL = ViLmax32u4 -.4
#ViH = ViHmin32u4 + .4
ViL = ViLmax328 -.4
ViH = ViHmin328 + .4

def parallel(R1, R2):
    return 1/(1/R1 + 1/(R2))
def voltageDivider(R1, R2, voltage):
    return R2 * voltage /(R1 + R2)

for R1 in range(1000, 10*1000*1000, 1000):
    for R2 in range(1000, 1*1000*1000, 1000):
        vo1 =  voltageDivider(R1, R2, Vin)
        vo2 =  voltageDivider(parallel(R1, Rpu), R2, Vin)
        vo22 = voltageDivider(parallel(R1, Rpu2), R2, Vin)

        if(vo1 < ViL and vo2 > ViH and vo22 > ViH):
            print("R1:{}, R2: {}, Vo1: {:.3}, Vo2: {:.3}, Vo22: {:.3}".format(R1, R2, vo1, vo2, vo22))



